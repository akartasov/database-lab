#--------------------------------------------------------------------------
# Copyright (c) 2019-2021, Postgres.ai, Nikolay Samokhvalov nik@postgres.ai
# All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
#--------------------------------------------------------------------------

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - when: always

# Environments.
.environment_production: &env_production
  environment: 
    name: production
    url: https://postgres.ai
  variables:
    ENV: production
    NAMESPACE: production
    DOCKER_NAME: "gcr.io/postgres-ai/platform-web/cloud"
    TAG: "${DOCKER_NAME}:${CI_COMMIT_TAG}-${CI_PIPELINE_IID}"

.environment_staging: &env_staging
  environment: 
    name: staging
    url: https://console-v2.postgres.ai
  variables:
    ENV: staging
    NAMESPACE: staging
    DOCKER_NAME: "gcr.io/postgres-ai/platform-web/cloud"
    TAG: "${DOCKER_NAME}:${NAMESPACE}-${CI_PIPELINE_IID}"

.environment_dev: &env_dev
  environment:
    name: dev
    url: https://console-dev.postgres.ai
  variables:
    ENV: dev
    NAMESPACE: dev
    DOCKER_NAME: "gcr.io/postgres-ai/platform-web/cloud"
    TAG: "${DOCKER_NAME}:${NAMESPACE}-${CI_PIPELINE_IID}"

# Jobs templates.
.build_definition: &build_definition
  stage: build
  image: docker:20.10.12
  services:
    - docker:dind
  script:
    - apk add --no-cache bash
    - bash ./web/packages/platform/ci_docker_build_push.sh

.deploy_definition: &deploy_definition
  stage: deploy
  image: dtzar/helm-kubectl:2.14.1
  script:
    # Substitute env variables in deploy config.
    - bash ./web/packages/platform/do.sh subs_envs ./web/packages/platform/deploy/platform-console.yaml /tmp/platform-console.yaml
    # Deploy to k8s cluster.
    - kubectl apply --filename /tmp/platform-console.yaml -n $NAMESPACE

# Conditions.
.only_web_tag_release: &only_web_tag_release
  rules:
    - if: $CI_COMMIT_TAG =~ /^web\/[0-9.]+$/

.only_web_staging: &only_web_staging
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - web/**/*

.only_web_feature: &only_web_feature
  rules:
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      changes:
        - web/**/*
      when: manual

# Jobs.
# Production.
build_cloud_image_tag_release:
  <<: *env_production
  <<: *only_web_tag_release
  <<: *build_definition

deploy_cloud_image_tag_release:
  <<: *env_production
  <<: *only_web_tag_release
  <<: *deploy_definition

# Staging.
build_cloud_image_staging:
  <<: *env_staging
  <<: *only_web_staging
  <<: *build_definition

deploy_cloud_image_staging:
  <<: *env_staging
  <<: *only_web_staging
  <<: *deploy_definition

# Dev.
build_cloud_image_dev:
  <<: *env_dev
  <<: *only_web_feature
  <<: *build_definition
  allow_failure: true # Workaround: https://gitlab.com/gitlab-org/gitlab/-/issues/20237

deploy_cloud_image_dev:
  <<: *env_dev
  <<: *only_web_feature
  <<: *deploy_definition
  allow_failure: true
  needs:
    - build_cloud_image_dev
